import React from 'react';

class Earthquake extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        {this.props.yndex === 0 && <h4>{`Earthquake ${this.props.index + 1}`}</h4>}
        <h6>{`${this.props.property[0].toUpperCase()}: ${this.props.property[1]}`}</h6>
      </div>
    );
  }
}

export default Earthquake;