import React from 'react';
import 'assets/scss/App.scss';
import MapContainer from 'components/MapContainer.jsx';
import { BrowserRouter, Route } from 'react-router-dom';

class App extends React.PureComponent {
  render() {
    return (
      <BrowserRouter>
        <div className="app">
          <Route exact path="/" component={MapContainer} />
        </div>
      </BrowserRouter>
    );
  }
}

export default App;
