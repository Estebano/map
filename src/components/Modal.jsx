import React from 'react';
import '../assets/scss/Modal.scss';

class Modal extends React.PureComponent {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div className={this.props.open ? "modal active" : "modal"}>
        <div className={"modal-content"}>
          <div className={'modal-close'} onClick={() => this.props.onClose()}>
            <img src="https://dictionary.cambridge.org/images/full/cross_noun_002_09265.jpg?version=4.0.82" />
          </div>
          <div className={'modal-data'}>
            <h2>Earthquake Properties</h2>
            {this.props.data}
          </div>
        </div>
      </div>
    );
  }
}

export default Modal;