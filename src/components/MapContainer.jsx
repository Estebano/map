import React from 'react';
import '../assets/scss/MapContainer.scss';
import { Map, GoogleApiWrapper, Marker } from 'google-maps-react';
import Modal from 'components/Modal.jsx';
import Earthquake from 'components/Earthquake.jsx';

class MapContainer extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      markers: [],
      earthquakes: [],
      bounds: null,
      modalData:  <p>No Content</p>,
      modalIsOpen: false
    };
    this.getData();
  }

  // Get earthquakes data asynchronously then set the markers
  getData() {
    fetch('https://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/all_day.geojson')
      .then(response => response.json())
      .then(data => {
        this.state.earthquakes = data;
        this.setMarkers();
      });
  }

  setBounds() {
    let bounds = new this.props.google.maps.LatLngBounds;
   this.state.markers.map(marker => {
      bounds.extend({ lat: marker.latitude, lng: marker.longitude });
    });
    this.setState({
      bounds: bounds
    });
  }

  setMarkers() {
    let markers = this.state.earthquakes.features;
    let _this = this;
    this.setState({
      markers: _this.getMarkersProperties(markers)
    });
    this.setBounds();
  }

  getMarkersProperties(markers) {
    return markers.map( marker => {
      return {
        time : new Date(marker.properties.time).toString(),
        latitude: marker.geometry.coordinates[0],
        longitude: marker.geometry.coordinates[1],
        depth: marker.geometry.coordinates[2],
        tsunami: marker.properties.tsunami === 0 ? 'No' : 'Yes',
        magnitude: marker.properties.mag
      }
    });
  }

  // Open or close modal
  toggleModal(marker, index) {
    marker = marker || false;
    index = index || 0;
    let data = this.state.modalData;
    let modalIsOpen = !this.state.modalIsOpen;

    // open modal
    if(marker && index) {
      data = this.setMarkerProperties(marker, index);
    }

    this.setState({
      modalData: data,
      modalIsOpen: modalIsOpen
    });
  }

  //Set market or modal Earthquake properties
  setMarkerProperties(marker, index) {
    return Object.entries(marker).map((property, yndex) =>  {
      return ( <Earthquake property={property} yndex={yndex} index={index}/> )
    });
  }


  render() {
    return (
      <div className={'container'}>
        <h1>All day world Earthquakes</h1>
        <hr/>
        <h2>List of earthquakes</h2>
        <div className={'marker-properties'}>
          { this.state.markers.map( (marker, index) => {
              return this.setMarkerProperties(marker, index);
          })}
        </div>
        <h2>Map of earthquakes</h2>
        <div className={'map-wrapper'}>
          <Map
            ref={'map'}
            google={this.props.google}
            bounds={this.state.bounds}
          >
            { this.state.markers.map( (marker, index) => {
                return (
                  <Marker onClick={(e) => this.toggleModal(marker, index)} position={{ lat: marker.latitude, lng: marker.longitude }}/>
                )})
            }
          </Map>
        </div>
        <Modal open={this.state.modalIsOpen} data={this.state.modalData} onClose={() => this.toggleModal()}/>
      </div>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: 'AIzaSyBISETb_Vt0WxkUfoST_oF82jWV6at47g8'
})(MapContainer);
